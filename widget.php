<?php
/**
 * Widget for Elmentor Cuestionario
 */
function WWTT_f()
{
    class WWTT extends \Elementor\Widget_Base {

        /**
         * Get widget name.
         *
         * Retrieve oEmbed widget name.
         *
         * @since 1.0.0
         * @access public
         *
         * @return string Widget name.
         */
        public function get_name() {
            return 'TwitterLink';
        }
    
        /**
         * Get widget title.
         *
         * Retrieve oEmbed widget title.
         *
         * @since 1.0.0
         * @access public
         *
         * @return string Widget title.
         */
        public function get_title() {
            return __( 'TwitterLink' );
        }
    
        /**
         * Get widget icon.
         *
         * Retrieve oEmbed widget icon.
         *
         * @since 1.0.0
         * @access public
         *
         * @return string Widget icon.
         */
        public function get_icon() {
            return 'eicon-form-horizontal';
        }
    
        /**
         * Get widget categories.
         *
         * Retrieve the list of categories the oEmbed widget belongs to.
         *
         * @since 1.0.0
         * @access public
         *
         * @return array Widget categories.
         */
        public function get_categories() {
            return [ 'general' ];
        }
    
        /**
         * Register oEmbed widget controls.
         *
         * Adds different input fields to allow the user to change and customize the widget settings.
         *
         * @since 1.0.0
         * @access protected
         */
        protected function _register_controls() {
           
            $this->start_controls_section(
                'content_section',
                [
                    'label' => __( 'Content', 'plugin-name' ),
                    'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
                ]
            );
    
            $repeater = new \Elementor\Repeater();
    
            $repeater->add_control(
                'list_title', [
                    'label' => __( 'Title', 'plugin-domain' ),
                    'type' => \Elementor\Controls_Manager::TEXT,
                    'default' => __( 'List Title' , 'plugin-domain' ),
                    'label_block' => true,
                ]
            );
    
            $this->add_control(
                'list',
                [
                    'label' => __( 'Repeater List', 'plugin-domain' ),
                    'type' => \Elementor\Controls_Manager::REPEATER,
                    'fields' => $repeater->get_controls(),
                    'default' => [],
                    'title_field' => '{{{ list_title }}}',
                ]
            );
    
            $this->end_controls_section();

            WWTT_add_control_style( "Title" , ".title" , $this , array(
                'padding' => true,
                'border' => true,
                'color' => true,
                'background' => true,
                'typography' => true,
            ) , 'Title');
        }
    
        /**
         * Render oEmbed widget output on the frontend.
         *
         * Written in PHP and used to generate the final HTML.
         *
         * @since 1.0.0
         * @access protected
         */
        protected function render() {
            $settings = $this->get_settings_for_display();
            $list = $settings['list'];
            ?>
            <div class="content">
                <div class="title" id="titleWWTT">
                </div>
                <a class="twitter" id="btnTwitterWWTT" target="_blank">
                    <img src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4KPHN2ZyB3aWR0aD0iMzlweCIgaGVpZ2h0PSIzOXB4IiB2aWV3Qm94PSIwIDAgMzkgMzkiIHZlcnNpb249IjEuMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayI+CiAgICA8dGl0bGU+R3JvdXAgMTA8L3RpdGxlPgogICAgPGcgaWQ9IjA2MTUyMF9EZXNrdG9wIiBzdHJva2U9Im5vbmUiIHN0cm9rZS13aWR0aD0iMSIgZmlsbD0ibm9uZSIgZmlsbC1ydWxlPSJldmVub2RkIj4KICAgICAgICA8ZyBpZD0iMS4wLUhvbWUiIHRyYW5zZm9ybT0idHJhbnNsYXRlKC01NzYuMDAwMDAwLCAtNDc0NS4wMDAwMDApIj4KICAgICAgICAgICAgPGcgaWQ9IkZvb3RlciIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMTkuMDAwMDAwLCA0NDIxLjAwMDAwMCkiPgogICAgICAgICAgICAgICAgPGcgaWQ9IlNvY2lhbC1JY29ucyIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoNTU3LjAwMDAwMCwgMzI0LjAwMDAwMCkiPgogICAgICAgICAgICAgICAgICAgIDxnIGlkPSJHcm91cC0xMCIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMC4wMDAwMDAsIDAuMDAwMDAwKSI+CiAgICAgICAgICAgICAgICAgICAgICAgIDxwYXRoIGQ9Ik0xNS4wNDc1MiwyOS4yNSBDMjQuMTA0MTYsMjkuMjUgMjkuMDU3NjY0LDIxLjc0NzExMyAyOS4wNTc2NjQsMTUuMjQwNjQzMyBDMjkuMDU3NjY0LDE1LjAyNzUzNjcgMjkuMDU3NjY0LDE0LjgxNTM5MDEgMjkuMDQzMzYsMTQuNjA0MjAzNCBDMzAuMDA3MDA4LDEzLjkwNzE5MTMgMzAuODM4ODQ4LDEzLjA0NDIwNTcgMzEuNSwxMi4wNTU1NjQxIEMzMC42MDEzNDQsMTIuNDUzNzQ3IDI5LjY0Nzk2OCwxMi43MTQ5NDY1IDI4LjY3MTg0LDEyLjgzMDIzNTIgQzI5LjY5OTcxMiwxMi4yMTQ5MTQgMzAuNDY5MTUyLDExLjI0NzEwMyAzMC44MzY2NCwxMC4xMDY4ODY5IEMyOS44NzAwMTYsMTAuNjgwNDUwOCAyOC44MTI2NzIsMTEuMDg0NTg1MyAyNy43MDk4MjQsMTEuMzAyMDExNiBDMjUuODQ1MTIsOS4zMTkyNTY1OCAyMi43MjU4ODgsOS4yMjM1NTA2MiAyMC43NDMwMDgsMTEuMDg4MjMzMSBDMTkuNDY0MTkyLDEyLjI5MDc0OTIgMTguOTIxNiwxNC4wODI3NjQzIDE5LjMxODU2LDE1Ljc5MjYwODUgQzE1LjM1OTQyNCwxNS41OTQxODkgMTEuNjcwNzIsMTMuNzI0MjI2OSA5LjE3MDQsMTAuNjQ4MjkyOCBDNy44NjM0NTYsMTIuODk4MTAyOSA4LjUzMTA0LDE1Ljc3NjE5MzUgMTAuNjk0ODgsMTcuMjIwOTk4NCBDOS45MTEyMzIsMTcuMTk3NzY3OSA5LjE0NDc2OCwxNi45ODYzODkyIDguNDYsMTYuNjA0NzE3MyBMOC40NiwxNi42NjcxMTMzIEM4LjQ2MDY3MiwxOS4wMTA5MDE1IDEwLjExMjgzMiwyMS4wMjk1NTgzIDEyLjQxMDQsMjEuNDkzNjg5IEMxMS42ODU1MDQsMjEuNjkxNDM2NSAxMC45MjQ4OTYsMjEuNzIwMzMwNyAxMC4xODcwNCwyMS41NzgxNjM3IEMxMC44MzIwNjQsMjMuNTgzOTU3MiAxMi42ODA3MzYsMjQuOTU3OTE4NiAxNC43ODczNiwyNC45OTc0NjgxIEMxMy4wNDM3MTIsMjYuMzY3NzgxNyAxMC44ODk3NiwyNy4xMTE2Mzg4IDguNjcyMTYsMjcuMTA5MzM1IEM4LjI4MDM4NCwyNy4xMDg1NjcgNy44ODg5OTIsMjcuMDg0ODU2NSA3LjUsMjcuMDM4Mjk5NCBDOS43NTE3NzYsMjguNDgzMjk2MyAxMi4zNzE5MDQsMjkuMjQ5NzEyIDE1LjA0NzUyLDI5LjI0NjE2MDIiIGlkPSJGaWxsLTEiIGZpbGw9IiMwMDAwMDAiPjwvcGF0aD4KICAgICAgICAgICAgICAgICAgICAgICAgPGNpcmNsZSBpZD0iT3ZhbC1Db3B5IiBzdHJva2U9IiMwMDAwMDAiIGN4PSIxOS41IiBjeT0iMTkuNSIgcj0iMTkiPjwvY2lyY2xlPgogICAgICAgICAgICAgICAgICAgIDwvZz4KICAgICAgICAgICAgICAgIDwvZz4KICAgICAgICAgICAgPC9nPgogICAgICAgIDwvZz4KICAgIDwvZz4KPC9zdmc+" alt="">
                </a>
                <a class="restok" id="restok" onclick="changeTitleTwitter()">
                    <img src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4KPHN2ZyB3aWR0aD0iMzlweCIgaGVpZ2h0PSIzOXB4IiB2aWV3Qm94PSIwIDAgMzkgMzkiIHZlcnNpb249IjEuMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayI+CiAgICA8dGl0bGU+R3JvdXAgMTE8L3RpdGxlPgogICAgPGcgaWQ9IjA2MTUyMF9EZXNrdG9wIiBzdHJva2U9Im5vbmUiIHN0cm9rZS13aWR0aD0iMSIgZmlsbD0ibm9uZSIgZmlsbC1ydWxlPSJldmVub2RkIj4KICAgICAgICA8ZyBpZD0iMS4wLUhvbWUiIHRyYW5zZm9ybT0idHJhbnNsYXRlKC01NzYuMDAwMDAwLCAtNDc5NC4wMDAwMDApIj4KICAgICAgICAgICAgPGcgaWQ9IkZvb3RlciIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMTkuMDAwMDAwLCA0NDIxLjAwMDAwMCkiPgogICAgICAgICAgICAgICAgPGcgaWQ9IlNvY2lhbC1JY29ucyIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoNTU3LjAwMDAwMCwgMzI0LjAwMDAwMCkiPgogICAgICAgICAgICAgICAgICAgIDxnIGlkPSJHcm91cC0xMSIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMC4wMDAwMDAsIDQ5LjAwMDAwMCkiPgogICAgICAgICAgICAgICAgICAgICAgICA8Y2lyY2xlIGlkPSJPdmFsIiBzdHJva2U9IiMwMDAwMDAiIGN4PSIxOS41IiBjeT0iMTkuNSIgcj0iMTkiPjwvY2lyY2xlPgogICAgICAgICAgICAgICAgICAgICAgICA8ZyBpZD0iR3JvdXAtOSIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoNy41MDAwMDAsIDkuNzUwMDAwKSIgc3Ryb2tlPSIjMjMxRjIwIiBzdHJva2Utd2lkdGg9IjMiPgogICAgICAgICAgICAgICAgICAgICAgICAgICAgPHBvbHlsaW5lIGlkPSJTdHJva2UtMSIgcG9pbnRzPSI4LjAxNjc0MTM1IDkuNTU1MTg3NSA0LjE0NTg3NTk2IDUuNjg0MzIyMTIgMC4yNzU3ODk0MjMgOS41NTUxODc1Ij48L3BvbHlsaW5lPgogICAgICAgICAgICAgICAgICAgICAgICAgICAgPHBvbHlsaW5lIGlkPSJTdHJva2UtMyIgcG9pbnRzPSI0LjE0NjEwOTYyIDUuOTAzNTY3MzEgNC4xNDYxMDk2MiAxNy44NDQ4MzY1IDE2LjA4NzM3ODggMTcuODQ0ODM2NSI+PC9wb2x5bGluZT4KICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxwb2x5bGluZSBpZD0iU3Ryb2tlLTUiIHBvaW50cz0iMTUuOTY2MzU3NyA4LjY3ODgzODQ2IDE5LjgzNzIyMzEgMTIuNTQ5NzAzOCAyMy43MDgwODg1IDguNjc4ODM4NDYiPjwvcG9seWxpbmU+CiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8cG9seWxpbmUgaWQ9IlN0cm9rZS03IiBwb2ludHM9IjE5LjgzNjk4OTQgMTIuMzMwMzgwOCAxOS44MzY5ODk0IDAuMzg5MTExNTM4IDcuODk1NzIwMTkgMC4zODkxMTE1MzgiPjwvcG9seWxpbmU+CiAgICAgICAgICAgICAgICAgICAgICAgIDwvZz4KICAgICAgICAgICAgICAgICAgICA8L2c+CiAgICAgICAgICAgICAgICA8L2c+CiAgICAgICAgICAgIDwvZz4KICAgICAgICA8L2c+CiAgICA8L2c+Cjwvc3ZnPg==" alt="" class="recycle">
                </a>
                <script>
                    btnTwitterWWTT = document.getElementById('btnTwitterWWTT')
                    titleWWTT = document.getElementById('titleWWTT')
                    idrestok = 0
                    titles = []
                    <?php
                    for ($i=0; $i < count($list); $i++) { 
                        echo "titles[$i]=`".$list[$i]["list_title"]."`;";
                    }
                    ?>
                    changeTitleTwitter = () => {
                        text = titles[idrestok++]
                        titleWWTT.innerHTML = text
                        btnTwitterWWTT.href=`https://twitter.com/intent/tweet?text=${encodeURI(text)}`
                        if (idrestok>=titles.length) {
                            idrestok = 0
                        }
                    }
                    changeTitleTwitter()
                </script>
            </div>
            <?php
        }
    
    }
}
add_action( 'elementor_pro/init', function() {
    WWTT_f();
    \Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \WWTT() );
});