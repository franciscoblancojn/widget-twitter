<?php
/*
Plugin Name: Widget Twitter
Plugin URI: https://startscoinc.com/es/
Description: Crea un  Widget para Elementor
Author: Startsco
Version: 0.1
Author URI: https://startscoinc.com/es/#
License: 
*/
/*
License...
Copyright 2020 Startsco, Inc.
*/

require_once plugin_dir_path( __FILE__ ) . 'widget.php';
require_once plugin_dir_path( __FILE__ ) . 'functions.php';